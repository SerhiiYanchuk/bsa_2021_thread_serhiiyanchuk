import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { Comment } from '../models/comment/comment';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { CommentService } from './comment.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class RateService {
    public constructor(private authService: AuthenticationService, private postService: PostService, private commentService: CommentService) {}

    public likePost(post: Post, currentUser: User) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: true,
            userId: currentUser.id
        };

        // update current array instantly
        let existReaction = innerPost.reactions.find((x) => x.user.id === currentUser.id);
        if (existReaction === undefined)
            innerPost.reactions = innerPost.reactions.concat({ isLike: true, user: currentUser });             
        else {
            innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
            if(!existReaction.isLike)
                innerPost.reactions = innerPost.reactions.concat({ isLike: true, user: currentUser });
        }
        
        return this.postService.ratePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
                if (existReaction !== undefined)
                    innerPost.reactions = innerPost.reactions.concat(existReaction);

                return of(innerPost);
            })
        );
    }

    public dislikePost(post: Post, currentUser: User) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: false,
            userId: currentUser.id
        };

        let existReaction = innerPost.reactions.find((x) => x.user.id === currentUser.id);
        if (existReaction === undefined) 
            innerPost.reactions = innerPost.reactions.concat({ isLike: false, user: currentUser });                      
        else {
            innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
            if(existReaction.isLike)
                innerPost.reactions = innerPost.reactions.concat({ isLike: false, user: currentUser });      
        }
            

        return this.postService.ratePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
                if (existReaction !== undefined)
                    innerPost.reactions = innerPost.reactions.concat(existReaction);

                return of(innerPost);
            })
        );
    }

    public likeComment(comment: Comment, currentUser: User) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: true,
            userId: currentUser.id
        };

        // update current array instantly
        let existReaction = innerComment.reactions.find((x) => x.user.id === currentUser.id);
        if (existReaction === undefined)
            innerComment.reactions = innerComment.reactions.concat({ isLike: true, user: currentUser });             
        else {
            innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id);
            if(!existReaction.isLike)
                innerComment.reactions = innerComment.reactions.concat({ isLike: true, user: currentUser });
        }
        
        return this.commentService.rateComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id);
                if (existReaction !== undefined)
                    innerComment.reactions = innerComment.reactions.concat(existReaction);

                return of(innerComment);
            })
        );
    }

    public dislikeComment(comment: Comment, currentUser: User) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: false,
            userId: currentUser.id
        };

        let existReaction = innerComment.reactions.find((x) => x.user.id === currentUser.id);
        if (existReaction === undefined) 
            innerComment.reactions = innerComment.reactions.concat({ isLike: false, user: currentUser });                      
        else {
            innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id);
            if(existReaction.isLike)
                innerComment.reactions = innerComment.reactions.concat({ isLike: false, user: currentUser });      
        }
            

        return this.commentService.rateComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id);
                if (existReaction !== undefined)
                    innerComment.reactions = innerComment.reactions.concat(existReaction);

                return of(innerComment);
            })
        );
    }
}
