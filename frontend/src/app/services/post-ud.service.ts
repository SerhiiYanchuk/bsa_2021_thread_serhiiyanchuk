import { Injectable } from '@angular/core';
import { Post } from '../models/post/post';
import { PostService } from './post.service';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PostUDService {
    public constructor(private postService: PostService) {}

    public deletePost(post: Post, posts: Post[]) {
        const copyPost: Post = JSON.parse(JSON.stringify(post));;

        const index: number = posts.indexOf(post);
        if (index !== -1) {
            posts.splice(index, 1);
        }   
      
        return this.postService.deletePost(post).pipe(
            map(() => copyPost),
            catchError(() => {
                // revert current array changes in case of any error
                posts.push(copyPost);
                return of(copyPost);
            })
        );
    }
}
