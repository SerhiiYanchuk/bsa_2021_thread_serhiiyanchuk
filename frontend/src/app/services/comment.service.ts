import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { NewComment } from '../models/comment/new-comment';
import { Comment } from '../models/comment/comment';
import { NewReaction } from '../models/reactions/newReaction';

@Injectable({ providedIn: 'root' })
export class CommentService {
    public routePrefix = '/api/comments';

    constructor(private httpService: HttpInternalService) {}

    public createComment(post: NewComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}`, post);
    }

    public rateComment(reaction: NewReaction) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}/rate`, reaction);
    }

    public updatePost(comment: Comment) {
        return this.httpService.putFullRequest<Comment>(`${this.routePrefix}`, comment)
    }

    public deletePost(comment: Comment) {
        return this.httpService.deleteFullRequest<Comment>(`${this.routePrefix}/${comment.id}`)
    }
}
