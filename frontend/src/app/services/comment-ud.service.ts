import { Injectable } from '@angular/core';
import { Comment } from '../models/comment/comment';
import { CommentService } from './comment.service';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CommentUDService {
    public constructor(private commentService: CommentService) {}

    public deletePost(comment: Comment, comments: Comment[]) {
        const copyComment: Comment = JSON.parse(JSON.stringify(comment));;

        const index: number = comments.indexOf(comment);
        if (index !== -1) {
            comments.splice(index, 1);
        }   

        return this.commentService.deletePost(comment).pipe(
            map(() => copyComment),
            catchError(() => {
                // revert current array changes in case of any error
                comments.push(copyComment);
                return of(copyComment);
            })
        );
    }
}
