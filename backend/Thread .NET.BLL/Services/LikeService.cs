﻿using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        public LikeService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task RatePost(NewReactionDTO reaction)
        {
            var like = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId).FirstOrDefault();

            if (like is not null)
            {
                _context.PostReactions.Remove(like);
                await _context.SaveChangesAsync();

                if ((reaction.IsLike && like.IsLike) || (!reaction.IsLike && !like.IsLike))
                    return; 
            }

            _context.PostReactions.Add(new DAL.Entities.PostReaction
            {
                PostId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }
        public async Task RateComment(NewReactionDTO reaction)
        {
            var like = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId).FirstOrDefault();

            if (like is not null)
            {
                _context.CommentReactions.Remove(like);
                await _context.SaveChangesAsync();

                if ((reaction.IsLike && like.IsLike) || (!reaction.IsLike && !like.IsLike))
                    return;
            }

            _context.CommentReactions.Add(new DAL.Entities.CommentReaction
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }
    }
}
